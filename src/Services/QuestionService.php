<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Session;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
        Session::set('questions_asked_ids', $this->json->encode([]));
    }

    /**
     * @return array|bool
     * @throws \KHerGe\JSON\Exception\DecodeException
     * @throws \KHerGe\JSON\Exception\EncodeException
     * @throws \KHerGe\JSON\Exception\UnknownException
     */
    public function getRandomQuestion()
    {
        $all_questions = $this->json->decodeFile(self::QUESTIONS_PATH, true);
        $random_key = array_rand($all_questions);
        $random_question = $all_questions[$random_key];

        if(isset($random_question['id'])){
            $questions_asked_ids = $this->json->decode(Session::get("questions_asked_ids"));

            if(!in_array($random_question['id'], $questions_asked_ids)){
                $questions_asked_ids[] = $random_question['id'];
                Session::set('questions_asked_ids', $this->json->encode($questions_asked_ids));

                return [
                    "id" => $random_question["id"],
                    "question" => $random_question["question"],
                    "possibleAnswers" => $random_question["possibleAnswers"],
                    "points" => $random_question["points"],
                ];
            }else{
                $this->getRandomQuestion();
            }
        }else{
            return false;
        }
    }

    /**
     * @param int $id
     * @param string $answer
     * @return int
     * @throws \KHerGe\JSON\Exception\DecodeException
     * @throws \KHerGe\JSON\Exception\UnknownException
     */
    public function getPointsForAnswer(int $id, string $answer): int
    {
        $all_questions = $this->json->decodeFile(self::QUESTIONS_PATH, true);
        $answered_question = [];

        foreach($all_questions as $question){
            if($question['id'] == $id){
                $answered_question = $question;
                break;
            }
        }

        if(isset($answered_question['correctAnswer']) && $answered_question['correctAnswer'] == $answer){
            return $answered_question['points'];
        }

        return 0;
    }
}