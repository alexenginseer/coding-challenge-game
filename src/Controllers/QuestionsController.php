<?php


namespace Ucc\Controllers;


use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    private QuestionService $questionService;

    /**
     * QuestionsController constructor.
     * @param QuestionService $questionService
     */
    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    /**
     * @return bool|false|string
     */
    public function beginGame(): string
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->error('You must provide a name', 400);
        }

        Session::set('name', $name);
        Session::set('questionCount', 1);
        Session::set('points', 0);

        $question = $this->questionService->getRandomQuestion();
        if($question){
            return $this->json(['question' => $question, 'game' => Session::get('name')], 201);
        }else{
            return $this->error('Some error happened!', 500);
        }
    }

    /**
     * @param int $id
     * @return bool|false|string
     * @throws \KHerGe\JSON\Exception\DecodeException
     * @throws \KHerGe\JSON\Exception\EncodeException
     * @throws \KHerGe\JSON\Exception\UnknownException
     */
    public function answerQuestion(int $id)
    {
        if (Session::get('name') === null) {
            return $this->error('You must first begin a game', 400);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->error('You must provide an answer', 400);
        }

        //check if answer is correct
        $points = $this->questionService->getPointsForAnswer($id, $answer);

        $totalPoints = Session::get('points');
        $totalPoints += $points;
        Session::set('points', $totalPoints);

        $message = $points > 0 ? 'Correct!' : "Wrong!";
        $questionCount = (int)Session::get('questionCount');
        $questionCount++;
        Session::set('questionCount', $questionCount);

        if ($questionCount > 5) {
            $name = Session::get('name');
            $points = $totalPoints;
            Session::destroy();
            return $this->success(['message' => $message." Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $question = $this->questionService->getRandomQuestion();

        return $this->json(['message' => $message, 'current_score' => $totalPoints, 'question' => $question, 'game' => Session::get('name')]);
    }
}