<?php


namespace Ucc\Http;


trait JsonResponseTrait
{
    /**
     * @param $data
     * @param int $statusCode
     * @return bool|false|string
     */
    public function json($data, int $statusCode = 200): string
    {
        http_response_code($statusCode);
        header('Session-Id: ' . session_id());
        header('Content-Type: application/json;charset=utf-8');

        return json_encode($data);
    }

    /**
     * @param $message
     * @param int $statusCode
     * @return bool|false|string
     */
    public function error($message, int $statusCode)
    {
        return $this->json(['message' => $message], $statusCode);
    }

    /**
     * @param $data
     * @param int $statusCode
     * @return bool|false|string
     */
    public function success($data)
    {
        return $this->json(['data' => $data], 200);
    }
}